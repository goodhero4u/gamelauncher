package mos.amazingdev;

import mos.amazingdev.loggers.CustomLogger;
import mos.amazingdev.rmi.SocketClient;
import mos.amazingdev.rmi.SocketServer;

import javax.swing.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void main(String[] args) {

        try {
            CustomLogger.setup();
            LOGGER.setLevel(Level.SEVERE);
        } catch (IOException e) {
            throw new RuntimeException("Problems with creating the log files");
        }

        if (SocketServer.clientIsUsingVPN()) {
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame, "Error! VPN detected! Please disconnect or disable your VPN in order to be run the game!\nIntegrity could be affected by VPN");
            LOGGER.severe("Client using VPN " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            System.exit(-1);
        } else {

            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("--version")) {
                    SocketClient socket = new SocketClient();
                    try {
                        socket.replyToServerWithLauncherVersion();
                    } catch (IllegalStateException exception) {
                        LOGGER.severe("Method that responds to server with version number has failed. Terminating!" + Thread.currentThread().getStackTrace()[1].getLineNumber());
                        System.exit(-1);
                    }


                } else {
                    LOGGER.severe("Invalid argument passed to launcher :" + args[0] + Thread.currentThread().getStackTrace()[1].getLineNumber());
                    System.exit(-1);
                }
            } else {
                try {
                    Launcher.launchStage(args);
                } catch (IllegalStateException exception) {
                    LOGGER.severe("A failure occurred with a method during application launch." + Thread.currentThread().getStackTrace()[1].getLineNumber());
                    System.exit(-1);
                }

            }
        }

    }
}