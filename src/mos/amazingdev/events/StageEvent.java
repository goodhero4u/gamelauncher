package mos.amazingdev.events;

import javafx.event.EventType;

import java.io.IOException;

public class StageEvent extends StageEventUpdater {

    public static final EventType<StageEventUpdater> CUSTOM_EVENT_TYPE_1 = new EventType(CUSTOM_EVENT_TYPE, "StageEvent");


    public StageEvent() {
        super(CUSTOM_EVENT_TYPE_1);
    }

    @Override
    public void invokeHandler(CustomHandler handler) {
        try {
            handler.onStageEvent();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}