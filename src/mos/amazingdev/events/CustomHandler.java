package mos.amazingdev.events;

import javafx.event.EventHandler;
import javafx.scene.control.Label;

import java.io.IOException;

public abstract class CustomHandler implements EventHandler<StageEventUpdater> {

    public abstract void onStageEvent() throws IOException, ClassNotFoundException;
    public abstract void onProcessChangeEvent(Label label, String newMessage);


    @Override
    public void handle(StageEventUpdater event) {
        event.invokeHandler(this);
    }
}
