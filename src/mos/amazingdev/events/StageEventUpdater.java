package mos.amazingdev.events;

import javafx.event.Event;
import javafx.event.EventType;

public abstract class StageEventUpdater extends Event {

    public static final EventType<StageEventUpdater> CUSTOM_EVENT_TYPE = new EventType(ANY);

    public StageEventUpdater(EventType<? extends Event> eventType) {
        super(eventType);
    }

    public abstract void invokeHandler(CustomHandler handler);

}
