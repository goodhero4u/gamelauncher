package mos.amazingdev.events;

import javafx.event.EventType;
import javafx.scene.control.Label;

 public class ChangeMessagEvent extends StageEventUpdater {

    public static final EventType<StageEventUpdater> CUSTOM_EVENT_TYPE_1 = new EventType(CUSTOM_EVENT_TYPE, "ProcessChangEvent");
    private final Label label;
    private final String newMessage;

    public ChangeMessagEvent(Label label, String newMessage) {
        super(CUSTOM_EVENT_TYPE_1);
        this.label = label;
        this.newMessage = newMessage;
    }

    @Override
    public void invokeHandler(CustomHandler handler) {
        handler.onProcessChangeEvent(label, newMessage);
    }

}