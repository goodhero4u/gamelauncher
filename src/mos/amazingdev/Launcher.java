package mos.amazingdev;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import mos.amazingdev.events.ChangeMessagEvent;
import mos.amazingdev.events.CustomHandler;
import mos.amazingdev.events.StageEvent;
import mos.amazingdev.events.StageEventUpdater;
import mos.amazingdev.rmi.SocketClient;
import mos.amazingdev.rmi.SocketServer;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static mos.amazingdev.RemoteFileExtractor.GAME_RUNNABLE_NAME;

public class Launcher extends Application {

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private RemoteFileExtractor remoteFileExtractor = new RemoteFileExtractor();
    private SocketServer server = new SocketServer();
    private ServerSocket serverSocketListen;
    private Group root = new Group();
    private GridPane gridpane = new GridPane();
    private static final String SUCCESS_MESSAGE = "Success";
    private static final String SKIPPED_MESSAGE = "Skipped!";
    private static final String NO_CONNECTION_MESSAGE = "No internet connection!";

    private boolean testMode;
    private Process processRun;

    public static Launcher getLauncherTest() {
        return new Launcher(true);
    }

    private Launcher(boolean testMode) {
        this.testMode = testMode;
    }

    public Launcher() {

    }

    @Override
    public void start(Stage primaryStage) {

        if (SocketServer.clientIsUsingVPN()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "VPN may interfere with the application's integrity.\nPlease disable your VPN \nand try to launch the application again!");
            alert.showAndWait();
            LOGGER.severe("Client using VPN " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            throw new IllegalStateException();
        }

        ProgressBar progressBar = new ProgressBar();
        progressBar.setId("progress");
        progressBar.setMinSize(300, 50);

        List<Labeled> launchStageMessages = new ArrayList<>(15);

        String processingMessage = "Processing . . .";

        Label step1 = new Label("Step 1: ===>");
        Label launchStage1 = new Label("Checking if game is already downloaded");
        Label resultOfStep1 = new Label(processingMessage);

        Label step2 = new Label("Step 2:  ===>");
        Label launchStage2 = new Label("Checking if game is latest version");
        Label resultOfStep2 = new Label(processingMessage);

        Label step3 = new Label("Step 3:  ===>");
        Label launchStage3 = new Label("Updating current game");
        Label resultOfStep3 = new Label(processingMessage);

        Label step4 = new Label("Step 4:  ===>");
        Label launchStage4 = new Label("Downloading game files");
        Label resultOfStep4 = new Label(processingMessage);

        Label step5 = new Label("Step 5:  ===>");
        Label launchStage5 = new Label("Launching game");
        Label resultOfStep5 = new Label(processingMessage);

        Label setupMessage = new Label("Updating and validating game. Please be patient...");
        setupMessage.setAlignment(Pos.BASELINE_RIGHT);

        launchStageMessages.add(step1);
        launchStageMessages.add(resultOfStep1);
        launchStageMessages.add(launchStage1);
        launchStageMessages.add(step2);
        launchStageMessages.add(resultOfStep2);
        launchStageMessages.add(launchStage2);
        launchStageMessages.add(step3);
        launchStageMessages.add(resultOfStep3);
        launchStageMessages.add(launchStage3);
        launchStageMessages.add(step4);
        launchStageMessages.add(resultOfStep4);
        launchStageMessages.add(launchStage4);
        launchStageMessages.add(step5);
        launchStageMessages.add(resultOfStep5);
        launchStageMessages.add(launchStage5);

        for (Labeled label : launchStageMessages) {
            label.setFont(Font.font(15.0));
        }

        gridpane.setAlignment(Pos.CENTER);
        gridpane.setHgap(40);
        gridpane.setVgap(20);

        gridpane.add(setupMessage, 1, 0, 3, 1);
        gridpane.add(progressBar, 1, 1, 3, 1);
        gridpane.add(step1, 1, 2);
        gridpane.add(launchStage1, 2, 2);
        gridpane.add(resultOfStep1, 3, 2);
        gridpane.add(step2, 1, 3);
        gridpane.add(launchStage2, 2, 3);
        gridpane.add(resultOfStep2, 3, 3);
        gridpane.add(step3, 1, 4);
        gridpane.add(launchStage3, 2, 4);
        gridpane.add(resultOfStep3, 3, 4);
        gridpane.add(step4, 1, 5);
        gridpane.add(launchStage4, 2, 5);
        gridpane.add(resultOfStep4, 3, 5);
        gridpane.add(step5, 1, 6);
        gridpane.add(launchStage5, 2, 6);
        gridpane.add(resultOfStep5, 3, 6);


        root.getChildren().add(gridpane);
        root.setAutoSizeChildren(true);

        Scene scene = new Scene(root, 700, 300);
        primaryStage.setTitle("Jack The Giant- Game Launcher");
        primaryStage.setScene(scene);
        primaryStage.show();

        SocketClient socket = new SocketClient();
        socket.replyToServerWithLauncherSuccess();


        primaryStage.addEventHandler(StageEventUpdater.CUSTOM_EVENT_TYPE, new CustomHandler() {

            @Override
            public void onProcessChangeEvent(Label label, String newMessage) {

                Platform.runLater(() -> label.setText(newMessage));

            }

            @Override
            public void onStageEvent() {

                if (gameRunnableAlreadyDownloaded()) {
                    primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep1, SUCCESS_MESSAGE));

                    if (internetIsConnected()) {

                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep2, "Checking game version"));

                        if (getExistingGameVersion() < getRemoteVersion()) {

                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep2, "Game outdated"));
                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep3, "Updating game"));
                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep4, "Downloading files"));
                            remoteFileExtractor.getGameRunnablefile();

                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep4, SUCCESS_MESSAGE));
                            double currentGameVersion = getExistingGameVersion();

                            if (currentGameVersion == getRemoteVersion()) {
                                primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep3, SUCCESS_MESSAGE + "-" + currentGameVersion));
                                primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep5, "Launching game!"));
                                launchGame();
                                if (gameLaunchIsSuccessful()) {
                                    resultOfStep5.setText(SUCCESS_MESSAGE);
                                    Platform.runLater(primaryStage::close);
                                }
                            } else {
                                Platform.runLater(() -> {
                                    Alert versionMismatch = new Alert(Alert.AlertType.ERROR, "Something went wrong during the update process!\n Please contact support with error code 1001", ButtonType.OK);
                                    versionMismatch.showAndWait();
                                    ErrorReporter.sendFeedback(String.valueOf(getExistingGameVersion()));
                                    ErrorReporter.sendFeedback("Version mismatch between artifact and remote version file");
                                    primaryStage.close();
                                    Platform.exit();

                                });
                            }


                        } else {
                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep2, "Game is up-to-udpate! " + getExistingGameVersion()));
                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep3, SKIPPED_MESSAGE));
                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep4, SKIPPED_MESSAGE));
                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep5, "Launching game!"));
                            launchGame();
                            if (gameLaunchIsSuccessful()) {
                                primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep5, SUCCESS_MESSAGE));
                                Platform.runLater(primaryStage::close);
                            }
                        }

                    } else {
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep1, NO_CONNECTION_MESSAGE));
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep2, NO_CONNECTION_MESSAGE));
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep3, NO_CONNECTION_MESSAGE));
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep4, NO_CONNECTION_MESSAGE));
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep4, "Launching game"));

                        launchGame();
                        if (gameLaunchIsSuccessful()) {
                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep5, SUCCESS_MESSAGE));

                            Platform.runLater(primaryStage::close);
                        }
                    }
                } else {
                    if (!internetIsConnected()) {
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep1, "Game files not found"));

                        Platform.runLater(() -> {
                            Alert noConnectionAlert = new Alert(Alert.AlertType.ERROR, "Internet connected required to obtain game files!\nPlease connect to the internet and try again!", ButtonType.OK);
                            noConnectionAlert.showAndWait();
                            primaryStage.close();
                            Platform.exit();

                        });

                    } else {
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep1, "Game files not found"));
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep2, SKIPPED_MESSAGE));
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep3, SKIPPED_MESSAGE));
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep4, "Downloading"));
                        remoteFileExtractor.getGameRunnablefile();

                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep4, SUCCESS_MESSAGE));
                        primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep5, "Launching game"));
                        launchGame();

                        if (gameLaunchIsSuccessful()) {
                            primaryStage.fireEvent(new ChangeMessagEvent(resultOfStep5, SUCCESS_MESSAGE));

                            Platform.runLater(primaryStage::close);
                        }
                    }
                }
            }
        });

        if (!testMode) {
            new Thread(() -> primaryStage.fireEvent(new StageEvent())).start();
        }

    }

    public static void launchStage(String[] args) {
        launch(args);
    }


    double getRemoteVersion() {
        double remoteLauncherVer = Double.parseDouble(remoteGameVersion());
        if (!(remoteLauncherVer > 0)) {
            LOGGER.severe("Failed to retrieve the remote game version since value received is invalid " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            JOptionPane.showMessageDialog(new JFrame(), "Failed to retrieve remote version. Please contact support", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            throw new IllegalStateException();
        }
        return remoteLauncherVer;

    }

    private String remoteGameVersion() {
        try {
            String text;
            try (BufferedReader brTest = new BufferedReader(new FileReader(remoteFileExtractor.getRemoteGameVersionFile()))) {
                text = brTest.readLine();
            }
            return text;
        } catch (IOException exception) {
            exception.printStackTrace();
            ErrorReporter.sendFeedback(exception.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR, "A fatal error occurred! Please contact support\nand provide them with error code 5001");
            alert.showAndWait();
            Platform.exit();
            LOGGER.severe(exception.getMessage() + " " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            throw new IllegalComponentStateException();

        }

    }

    boolean gameRunnableAlreadyDownloaded() {
        File gameFile = new File(GAME_RUNNABLE_NAME);
        return gameFile.exists() && ((gameFile.length() / 1024) > 3000);
    }

    public void launchGame() {
        try {
            serverSocketListen = SocketServer.getServerSocketInstance(15);
            server.writeFreePortNumberToTempDir(serverSocketListen);
            processRun = Runtime.getRuntime().exec(new String[]{"java", "-jar", GAME_RUNNABLE_NAME});
        } catch (IOException exception) {
            exception.printStackTrace();
            ErrorReporter.sendFeedback(exception.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR, "A fatal error occurred! Please contact support\nand provide them with error code 6001");
            alert.showAndWait();
            Platform.exit();
            LOGGER.severe(exception.getMessage() + " " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            throw new IllegalComponentStateException();
        }
    }

    public void launchGame(String argument) {
        try {
            serverSocketListen = SocketServer.getServerSocketInstance(15);
            server.writeFreePortNumberToTempDir(serverSocketListen);
            processRun = Runtime.getRuntime().exec(new String[]{"java", "-jar", GAME_RUNNABLE_NAME, argument});
        } catch (IOException exception) {
            exception.printStackTrace();
            ErrorReporter.sendFeedback(exception.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR, "A fatal error occurred! Please contact support\nand provide them with error code 6001");
            alert.showAndWait();
            Platform.exit();
            LOGGER.severe(exception.getMessage() + " " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            throw new IllegalComponentStateException();
        }
    }

    double getExistingGameVersion() {
        launchGame("--version");
        double gameVersion = -1;
        Object[] responses = server.getResponseFromServer(serverSocketListen);
        if (responses[0] == null) {
            LOGGER.severe("Failed to retrieve downloaded game's version after timeout or the launcher returned wrong value " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            JOptionPane.showMessageDialog(new JFrame(), "Failed to retrieve game's version. Please contact support", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            throw new IllegalStateException();
        } else {
            try {
                gameVersion = Double.parseDouble(responses[0].toString());
            } catch (NumberFormatException exception) {
                LOGGER.severe("The game version is not in the correct format or is not a double" + Thread.currentThread().getStackTrace()[1].getLineNumber());
            }
        }
        return gameVersion;
    }

    boolean gameLaunchIsSuccessful() {
        boolean launchSuccessful = false;
        Object[] responses = server.getResponseFromServer(serverSocketListen);
        if (responses[0] != null) {
            if (responses[0].toString().equalsIgnoreCase("GAME-LIVE")) {
                launchSuccessful = true;
            }
        }
        if (!launchSuccessful) {
            LOGGER.severe("Game launch has failed " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            throw new IllegalStateException();
        }

        return true;
    }

    boolean gameLaunchIsSuccessful(boolean closeGameAfterConfirmation) {
        boolean launchSuccessful = false;
        Object[] responses = server.getResponseFromServer(serverSocketListen);
        if (responses[0] != null) {
            if (responses[0].toString().equalsIgnoreCase("GAME-LIVE")) {
                launchSuccessful = true;
            }
        }
        if (!launchSuccessful) {
            LOGGER.severe("Game launch has failed " + Thread.currentThread().getStackTrace()[1].getLineNumber());
            throw new IllegalStateException();
        }
        if (closeGameAfterConfirmation) {
            processRun.destroy();
        }
        return true;
    }

    boolean internetIsConnected() {
        try {
            URL url = new URL("https://www.dropbox.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public Process getProcessRun() {
        return processRun;
    }
}