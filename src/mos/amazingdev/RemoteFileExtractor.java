package mos.amazingdev;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class RemoteFileExtractor {

    final static String GAME_VERSION_FILE_URL = "https://www.dropbox.com/sh/4ew5iqa2jtvy43s/AABkPbkswo1E1IcoK2K15mdTa?dl=1";
    final static String GAME_RUNNABLE_URL = "https://www.dropbox.com/sh/lub6fpxn0q2ismx/AACc4IAotQjezin1aOV2CX_ea?dl=1";
    public final static String GAME_RUNNABLE_NAME = "JackGiantRunnable.jar";
    final static String GAME_VERSION_FILE_NAME = "game-version.txt";
    final static String GAME_VERSION_ARCHIVE_NAME = "gameVersion.zip";
    final static String GAME_RUNNABLE_ZIP_NAME = "gameJarJack.zip";

    private File downloadArchiveFromDropbox(String url, String destinationArchiveName) throws IOException {

        File versionFileArchive = new File(destinationArchiveName);
        try (ReadableByteChannel readChannel = Channels.newChannel(new URL(url).openStream())) {
            try (FileOutputStream fileOS = new FileOutputStream(destinationArchiveName)) {
                try (FileChannel writeChannel = fileOS.getChannel()) {
                    writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);
                }

            }
        }
        return versionFileArchive;
    }

    private File getUnzippedFile(File file) {
        try {
            ZipFile zipFile = new ZipFile(file.getAbsoluteFile());
            ZipParameters parameters = new ZipParameters();
            parameters.setSourceExternalStream(true);
            List<FileHeader> headers = zipFile.getFileHeaders();
            String fileNameInArchive = headers.get(1).getFileName();
            zipFile.extractFile(fileNameInArchive, ".");
            deleteFile(file.getName());
            return new File(fileNameInArchive);
        } catch (ZipException | IOException | InterruptedException exception) {
            exception.printStackTrace();
            Platform.runLater(() -> {

                Alert alert = new Alert(Alert.AlertType.ERROR, "A fatal error occured! Please contact support\nand provide them with error code 2001");
                alert.showAndWait();
                Platform.exit();
            });
            ErrorReporter.sendFeedback(exception.getMessage());

            throw new IllegalComponentStateException();
        }
    }

    void deleteFile(String fileName) throws IOException, InterruptedException {
        Process deleteFileProcess = Runtime.getRuntime().exec(new String[]{"cmd", "/c", "del", "/f", fileName});
        deleteFileProcess.waitFor();
    }

    private void renameFile(File file, String newFileName) throws IOException, InterruptedException {
        Runtime.getRuntime().exec(new String[]{"cmd", "/c", "ren", file.getName(), newFileName}).waitFor();
        try (Stream<Path> walk = Files.walk(Paths.get("."))) {

            List<String> result = walk.filter(Files::isRegularFile)
                    .map(x -> x.getFileName().toString()).collect(Collectors.toList());

            if (!result.contains(newFileName)) {
                JOptionPane.showMessageDialog(new JFrame(), "A fatal error occured, please contact support with error code 2005", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
                throw new IllegalComponentStateException();
            }
        }
    }

    File getRemoteGameVersionFile() {
        try {
            deleteFile(GAME_VERSION_FILE_NAME);
            File gameVersionFile = getUnzippedFile(downloadArchiveFromDropbox(GAME_VERSION_FILE_URL, GAME_VERSION_ARCHIVE_NAME));
            renameFile(gameVersionFile, GAME_VERSION_FILE_NAME);
            return gameVersionFile;
        } catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
            ErrorReporter.sendFeedback(exception.getMessage());
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR, "A fatal error occured! Please contact support\nand provide them with error code 3001");
                alert.showAndWait();
                Platform.exit();
            });
            throw new IllegalComponentStateException();
        }
    }

    void getGameRunnablefile() {
        try {
            deleteFile(GAME_RUNNABLE_NAME);
            File gameRunnable = getUnzippedFile(downloadArchiveFromDropbox(GAME_RUNNABLE_URL, GAME_RUNNABLE_ZIP_NAME));
            renameFile(gameRunnable, GAME_RUNNABLE_NAME);
        } catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR, "A fatal error occured! Please contact support\nand provide them with error code 4001");
                alert.showAndWait();
                Platform.exit();
            });
            ErrorReporter.sendFeedback(exception.getMessage());

            throw new IllegalComponentStateException();
        }
    }
}