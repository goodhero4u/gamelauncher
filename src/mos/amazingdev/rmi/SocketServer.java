package mos.amazingdev.rmi;

import java.io.*;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Collections;
import java.util.TimerTask;
import java.util.regex.Pattern;

public final class SocketServer {

    final static String FREE_PORT_FILE_NAME = "JackLauncherPort.txt";

    public static ServerSocket getServerSocketInstance(int timeOutInSeconds) throws IOException {
        ServerSocket serverSocket = new ServerSocket(0);
        serverSocket.setSoTimeout(timeOutInSeconds * 1000);
        return serverSocket;
    }

    public Object[] getResponseFromServer( ServerSocket serverSocket) {
        Object[] responses = new Object[1];
        try (ServerSocket server = serverSocket) {
            writeFreePortNumberToTempDir(server);
            try (Socket socket = server.accept()) {
                try (ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) {
                    responses[0] = ois.readObject();

                }
                try (ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream())) {
                    if (responses[0] != null) {
                        oos.writeObject("OK");
                    }
                }
            }

        } catch (IOException | ClassNotFoundException exception) {
            return responses;
        }
        return responses;
    }

    public void writeFreePortNumberToTempDir(ServerSocket serverSocket) throws FileNotFoundException {
        File portNumberFile = new File(FREE_PORT_FILE_NAME);
        try (PrintWriter printWriter = new PrintWriter(portNumberFile)) {
            printWriter.println(serverSocket.getLocalPort());
        }
    }

    public static boolean clientIsUsingVPN() {
        Pattern tapNamePattern = Pattern.compile("tap", Pattern.CASE_INSENSITIVE);
        Pattern tunNamePattern = Pattern.compile("tun", Pattern.CASE_INSENSITIVE);
        final boolean[] isVPN = {false};
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                try {
                    for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                        if (networkInterface.isUp()) {
                            if (tapNamePattern.matcher(networkInterface.toString()).find() || tunNamePattern.matcher(networkInterface.toString()).find()) {
                                isVPN[0] = true;
                                break;
                            }
                        }
                    }
                } catch (SocketException exception) {
                    exception.printStackTrace();
                    isVPN[0] = true;
                }
            }
        };
        timerTask.run();
        return isVPN[0];
    }
}