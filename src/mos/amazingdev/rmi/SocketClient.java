package mos.amazingdev.rmi;

import javafx.application.Platform;
import mos.amazingdev.ErrorReporter;

import javax.swing.*;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Logger;

public class SocketClient {

    private static final String PATH_TO_PORT_NUMBER_FILE = "JackUpdaterPort.txt";
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    static final String LAUNCHER_VERSION = "1.2";

    private int getPortNumber() {
        try {
            int portNumber = -1;
            File file = new File(PATH_TO_PORT_NUMBER_FILE);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String textContent;
            while ((textContent = br.readLine()) != null) {

                portNumber = Integer.parseInt(textContent);
            }
            return portNumber;
        } catch (Exception exception) {
            ErrorReporter.sendFeedback(exception.getMessage());
            throw new IllegalAccessError();
        }

    }

    public void replyToServerWithLauncherVersion() {
        boolean responseReceived = false;
        try {
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;
            ObjectOutputStream oos;
            ObjectInputStream ois;
            int portNumber = getPortNumber();
            socket = new Socket(host.getHostName(), portNumber);
            socket.setSoTimeout(20 * 1000);
            oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(LAUNCHER_VERSION);
            ois = new ObjectInputStream(socket.getInputStream());
            String message = (String) ois.readObject();
            if (message.equalsIgnoreCase("ok")) {
                ois.close();
                oos.close();
                responseReceived = true;
            }

            if (!responseReceived) {
                throw new IOException("Response wait timed out.");
            }

        } catch (IOException | ClassNotFoundException exception) {
            LOGGER.severe("Could not receive response from server side." + Thread.currentThread().getStackTrace()[1].getLineNumber());
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame, "A communication error occurred Contact support with error code 7000");
            Platform.exit();
            throw new IllegalStateException();
        }


    }

    public void replyToServerWithLauncherSuccess() {
        boolean resopnseReceived = false;
        try {
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;
            ObjectOutputStream oos;
            ObjectInputStream ois;
            socket = new Socket(host.getHostName(), getPortNumber());
            socket.setSoTimeout(20 * 1000);
            oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject("LAUNCHER-LIVE");
            ois = new ObjectInputStream(socket.getInputStream());
            String message = (String) ois.readObject();
            if (message.equalsIgnoreCase("ok")) {
                ois.close();
                oos.close();
                resopnseReceived = true;
            } else if (message.equalsIgnoreCase("exit")) {
                ois.close();
                oos.close();
                resopnseReceived = true;
            }


            if (!resopnseReceived) {
                throw new IOException("Response wait timed out");
            }

        } catch (IOException | ClassNotFoundException exception) {
            LOGGER.severe("Method threw an exception " + exception.getMessage() + Thread.currentThread().getStackTrace()[1].getLineNumber());
            LOGGER.severe("Could not receive response from server side." + Thread.currentThread().getStackTrace()[1].getLineNumber());
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame, "A communication error occurred Contact support with error code 7000");
            Platform.exit();
            throw new RuntimeException();

        }
    }
}