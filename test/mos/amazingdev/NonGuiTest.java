package mos.amazingdev;

import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.fail;

public class NonGuiTest {

    Process process;

    @Test
    public void launchGameRunnableisWorking() {
        Launcher launcher = new Launcher();
        RemoteFileExtractor remoteFileExtractor = new RemoteFileExtractor();
        remoteFileExtractor.getGameRunnablefile();
        launcher.launchGame();
        process = launcher.getProcessRun();
        assert (launcher.gameLaunchIsSuccessful(true));
    }

    @Test
    public void checkGameVersionQueryIsWorking() {
        Launcher launcher = new Launcher();
        double gameVersion = launcher.getExistingGameVersion();
        assert (gameVersion != -1 && (Double) gameVersion instanceof Double && gameVersion > 0);

    }

    @Test
    public void checkRemoteGameVersion() {
        Launcher launcher = new Launcher();
        assert (launcher.getRemoteVersion() != -1);

    }

    @Test
    public void getRemoteGameIsWorking() {
        try {
            RemoteFileExtractor remoteFileExtractor = new RemoteFileExtractor();
            remoteFileExtractor.getGameRunnablefile();
        } catch (Exception exception) {
            fail(exception.getMessage());
        }
    }

    @After
    public void tearDown() {
        if (process != null) {
            process.destroy();
        }
    }
}