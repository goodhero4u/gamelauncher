package mos.amazingdev;

import javafx.scene.Node;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Test;
import org.testfx.api.FxAssert;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.matcher.base.NodeMatchers;
import org.testfx.service.query.EmptyNodeQueryException;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.fail;

public class GuiTest extends ApplicationTest {

    private Stage stage;
    private Launcher launcher;

    @Override
    public void start(Stage stage) {
        launcher = Launcher.getLauncherTest();
        this.stage = stage;
        launcher.start(stage);
    }


    @Test
    public void stageIsShowingWhenAppLaunches() {
        assert (stage.isShowing());
    }

    @Test
    public void progressScreenIsShowing() {
        try {
            Node progressIndicator = lookup("#progress").query();
            FxAssert.verifyThat(progressIndicator, NodeMatchers.isVisible());
        } catch (EmptyNodeQueryException exception) {
            fail("Case failed since node doesn't exist yet");
            exception.printStackTrace();
        }
    }

    @After
    public void teardown() {
        try {
            if (launcher.getProcessRun() != null) {
                launcher.getProcessRun().destroy();
            }

            FxToolkit.hideStage();
        } catch (TimeoutException e) {
            e.printStackTrace();
            fail("Teardown failed since the game launched is not ending process");
        }
    }
}